package com.raynhl.intelmsp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.CursorIndexOutOfBoundsException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.raynhl.intelmsp.CardAdapter;
import com.raynhl.intelmsp.Config;
import com.raynhl.intelmsp.Data;
import com.raynhl.intelmsp.DatabaseHandler;
import com.raynhl.intelmsp.JsonParser;
import com.raynhl.intelmsp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private DatabaseHandler db;
    private Config config;
    private Handler handler;
    private Runnable runnable;
    private String[] mDrawerTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private SimpleAdapter mAdapter;
    private List<HashMap<String, String>> mList;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar toolbar;

    final private String DRAWER_NAME = "name";
    final private String DRAWER_ICON = "icon";

    private int mDrawerIcons[] = {
            R.drawable.home,
            R.drawable.toilet,
            R.drawable.pregnant,
            R.drawable.meeting,
            R.drawable.car
    };

    // how long before next data update
    public int refreshRate = 500 * 1000;

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

        private void selectItem(int position) {
            setActionBarTitle(mDrawerTitles[position]);
            showData(position);
            mDrawerLayout.closeDrawer(mDrawerList);
        }

        private void setActionBarTitle(String title) {
            setTitle(title);
        }
    }

    public void showFragment(int position) {

    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setting up the toolbar
        setUpToolbar();

        // Set up the navigation Drawer
        setUpNavigationDrawer();


        db = new DatabaseHandler(this);

        // Setting up the recyclerview
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        handler = new Handler();

        runnable = new Runnable() {
            @Override
            public void run() {
                Log.d("TIMER", "Periodically updating data");
                getData();
                handler.postDelayed(this,refreshRate);
            }
        };

        handler.postDelayed(runnable, 20);



    }

    public void setUpToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    public void setUpNavigationDrawer() {
        mDrawerTitles = getResources().getStringArray(R.array.drawer_list_items);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mList = new ArrayList<HashMap<String, String>>();

        String[] source = {DRAWER_NAME, DRAWER_ICON};
        int[] destination = {R.id.drawer_text, R.id.drawer_item_icon};

        for (int i=0; i < mDrawerTitles.length; i++) {
            HashMap<String, String> map = new HashMap<String,String>();
            map.put(DRAWER_NAME,mDrawerTitles[i]);
            map.put(DRAWER_ICON, Integer.toString(mDrawerIcons[i]));
            mList.add(map);
        }

        mAdapter = new SimpleAdapter(this,mList,R.layout.drawer_list_layout, source, destination);

        mDrawerToggle = new ActionBarDrawerToggle(this,
                                                mDrawerLayout,
                                                R.string.drawer_open,
                                                R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                Log.d("NAV", "Drawer Closed");
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d("NAV", "Drawer Opened");
            }
        };

//        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_layout, R.id.drawer_text, mDrawerTitles));
        mDrawerList.setAdapter(mAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // Set ActionBar title to "Home"
        setTitle(mDrawerTitles[0]);
        mDrawerList.setItemChecked(0,true);

        mDrawerLayout.addDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mDrawerToggle.syncState();

    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_refresh){
            Toast.makeText(MainActivity.this, "Fetching new data.", Toast.LENGTH_SHORT).show();
            getData();
            return super.onOptionsItemSelected(item);
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);




    }

    public void getData() {
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, config.GET_URL, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        JsonParser jsonParser = new JsonParser();
                        config = jsonParser.parse(response);

                        if (config != null) {
                            String timeStamp = jsonParser.getTimeNow();
                            Data data = new Data(1, timeStamp, response.toString());
                            db.addOrUpdateData(data);
                            showData(mDrawerList.getCheckedItemPosition());
                        }

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        try {
                            Data data = db.getData(1);
                            JsonParser jsonParser = new JsonParser();

                            config = jsonParser.parse(new JSONObject(data.getJsonData()), data.getTimeStamp());

                            if (config != null) {
                                showData(mDrawerList.getCheckedItemPosition());
                            }

                        } catch (CursorIndexOutOfBoundsException cursorError) {
                            Log.d("Cursor", "Empty DB");
                        }

                        catch (JSONException err) {
                            err.printStackTrace();
                            Log.d("Json", "something wrong with json");
                        }

                        displayErrorToast();
                    }
                });

        // Fix this. Need to set proper timeout.
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(
                1000000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Possibly need to do some timeout configuration here
        queue.add(jsObjRequest);
    }

    public void showData(int position) {
        String[] availableFacilityTypes = getResources().getStringArray(R.array.drawer_list_items);

        adapter = new CardAdapter(Config.facilityTypes, Config.facilityIcons, Config.facilitySubIcons,
                Config.availables, Config.buildingNames, Config.levels,
                Config.timeStamps, Config.cardBackgroundColors, availableFacilityTypes, position);
        recyclerView.setAdapter(adapter);
    }

    private void displayErrorToast() {
        CharSequence errorMessage = "Server access failure. Please check your internet connection.";
        Toast toast = Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG);
        toast.show();
    }

}
