package com.raynhl.intelmsp;

import android.graphics.Color;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by raynhl on 1/23/17.
 */

public class JsonParser {
    String LIGHT_GREEN    = "#8BC24A";
    String AMBER          = "#FFC928";
    String DEEP_ORANGE    = "#FE5722";
    String GREY           = "#9E9E9E";

    public Config parse(JSONObject jsonObjectMain) {
        try {
            JSONArray array = jsonObjectMain.getJSONArray(Config.TAG_JSON_ARRAY);


            Config config = new Config(array.length());
            for (int i = 0; i < array.length(); i++) {

                JSONObject jsonObject = array.getJSONObject(i);

                config.buildingNames[i] = getBuildingName(jsonObject);
                config.levels[i]        = getLevel(jsonObject);
                config.timeStamps[i]    = getTimeNow();

                String facilityType = getFacilityType(jsonObject);

                config.facilityTypes[i] = facilityType;
                config.availables[i] = getAvailable(jsonObject);
                config = setCardBackgroundColor(config,i,getTotal(jsonObject),jsonObject);

                // Segregate raw data into facilities type
                switch(facilityType) {
                    case "Restroom":
                        config = parseForRestroom(config, i, jsonObject);
                        break;

                    case "Mother's Room":
                        config = parseForMotherRoom(config, i, jsonObject);
                        break;

                    case "Carpark":
                        config = parseForCarpark(config, i, jsonObject);
                        config.levels[i] = getLevelToArea(jsonObject);
                        break;

                    case "Conference Room":
                        config = parseForConferenceRoom(config, i, jsonObject);
                        break;
                }


            }

            return config;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Config parse(JSONObject jsonObjectMain, String timeStamp) {
        try {
            JSONArray array = jsonObjectMain.getJSONArray(Config.TAG_JSON_ARRAY);


            Config config = new Config(array.length());
            for (int i = 0; i < array.length(); i++) {

                JSONObject jsonObject = array.getJSONObject(i);

                config.buildingNames[i] = getBuildingName(jsonObject);
                config.levels[i]        = getLevel(jsonObject);
                config.timeStamps[i]    = timeStamp;

                String facilityType = getFacilityType(jsonObject);

                config.facilityTypes[i] = facilityType;
                config.availables[i] = getAvailable(jsonObject);
                config = setCardBackgroundColor(config,i,getTotal(jsonObject),jsonObject);

                // Segregate raw data into facilities type
                switch(facilityType) {
                    case "Restroom":
                        config = parseForRestroom(config, i, jsonObject);
                        break;

                    case "Mother's Room":
                        config = parseForMotherRoom(config, i, jsonObject);
                        break;

                    case "Carpark":
                        config = parseForCarpark(config, i, jsonObject);
                        config.levels[i] = getLevelToArea(jsonObject);
                        break;

                    case "Conference Room":
                        config = parseForConferenceRoom(config, i, jsonObject);
                        break;
                }
            }

            return config;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getLevel(JSONObject j) {
        int level = 0;
        try {
            level = j.getInt(Config.TAG_LEVEL);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String levelString = "Level " + Integer.toString(level);
        return levelString;
    }

    private String getLevelToArea(JSONObject j) {
        int level = 0;
        try {
            level = j.getInt(Config.TAG_LEVEL);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        char area = (char) (level + 64);
        String levelString = "Carpark " + Character.toString(area);
        return levelString;
    }

    public String getTimeNow() {
        String hourString;
        String minuteString;

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        if (hour < 10) {
            hourString = "0" + Integer.toString(hour);
        }

        else {
            hourString = Integer.toString(hour);
        }

        if (minute < 10) {
            minuteString = "0" + Integer.toString(minute);
        }

        else {
            minuteString = Integer.toString(minute);
        }

        String timeStamp = "Last updated on " + hourString + ":" + minuteString;
        return timeStamp;
    }

    private Config setFacilityIcon(Config config, int index, int facilityIcon) {
        config.facilityIcons[index] = facilityIcon;
        return config;
    }

    private String getBuildingName(JSONObject j) {
        String buildingName = null;
        try {
            buildingName = j.getString(Config.TAG_BUILDING_NAME);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return buildingName;
    }

    private String getFacilityType(JSONObject j) {
        String facilityType = null;
        try {
            facilityType = j.getString(Config.TAG_FACILITY_TYPE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return facilityType;
    }

    private int getAvailable(JSONObject j) {
        int available = 0;
        try {
            available = j.getInt(Config.TAG_AVAILABLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return available;
    }

    private int getTotal(JSONObject j) {
        int total = 0;
        try {
            total = j.getInt(Config.TAG_TOTAL);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return total;
    }

    private Config parseForRestroom(Config config, int index, JSONObject j) {
        config = setFacilityIcon(config,index, R.drawable.toilet);
        config = setGenderIcon(config,index,j);

        return config;
    }

    private Config parseForMotherRoom(Config config, int index, JSONObject j) {
        config = setFacilityIcon(config,index, R.drawable.pregnant);
        config.facilitySubIcons[index] = R.drawable.pregnant;

        return config;
    }

    private Config parseForCarpark(Config config, int index, JSONObject j) {
        config = setFacilityIcon(config,index, R.drawable.car);
        config.facilitySubIcons[index] = R.drawable.car;

        return config;
    }

    private Config parseForConferenceRoom(Config config, int index, JSONObject j) {
        config = setFacilityIcon(config,index, R.drawable.meeting);
        config.facilitySubIcons[index] = R.drawable.meeting;

        return config;
    }

    private Config setGenderIcon(Config config, int index, JSONObject j) {
        int gender = getGender(j);
        if (gender == 0) {
            config.facilitySubIcons[index] = R.drawable.male;
        }

        else if (gender == 1) {
            config.facilitySubIcons[index] = R.drawable.female;
        }

        return config;
    }

    private int getGender(JSONObject j) {
        String gender = null;
        try {
            gender = j.getString(Config.TAG_GENDER);
            if (gender.equals("male")) {
                return 0;
            }

            else {
                return 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private Config setCardBackgroundColor(Config config, int index,int total, JSONObject j) {
        if (isUnderMaintenance(j)) {
            config.cardBackgroundColors[index] = Color.parseColor(GREY);
            config.availables[index] = 0;
        }

        else {
            int occupancyLevel = getOccupancyLevel(config.availables[index], total);

            switch(occupancyLevel) {
                case 1:
                    config.cardBackgroundColors[index] = Color.parseColor(LIGHT_GREEN);
                    break;
                case 2:
                    config.cardBackgroundColors[index] = Color.parseColor(AMBER);
                    break;
                case 3:
                    config.cardBackgroundColors[index] = Color.parseColor(DEEP_ORANGE);
                    break;
            }
        }

        return config;
    }

    private boolean isUnderMaintenance(JSONObject j) {
        boolean maintenance = false;

        try {
            maintenance = j.getBoolean(Config.TAG_MAINTENANCE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return maintenance;
    }

    private int getOccupancyLevel(int available, int total) {
        double percentage = (double) available / (double) total;

        if (percentage < 0.35) {
            return 3;
        }

        else if (percentage < 0.70) {
            return 2;
        }

        else {
            return 1;
        }
    }
}
